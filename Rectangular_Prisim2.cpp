/* 
 * File:   Rectangular_Prisim2.cpp
 * Suzuka/Emma
 * Created on November 4, 2017
 * code reulsed and repurposed from unit 7 ligthing using Cube
 * by stuetzlec's cube class by StuetzleC
 * FINAL PROJECT
 * Shaila DiGiovanni & Emma Theberge
 */
#include "Rectangular_Prisim2.h"

Rectangular_Prisim2::Rectangular_Prisim2()
{
    assert(false&&"Do not use default Rectangular_Prisim2 constructor.");
}

//where _f is the bottom left cornner

Rectangular_Prisim2::Rectangular_Prisim2(vec3 _f, double _length, double _width, double _depth, vec3 fC, vec3 bC, Texture* tex1)
{
    fillColor=fC;
    borderColor=bC;
    length=_length;
    width=_width;
    depth=_depth;
    texture1=tex1;
    anchorPoint = _f;
    //f, e, g, h, b, c, d, a
    boundingbox = new BoundingBox(_f, length, width, depth);

	printf("base vertex is x: %f y: %f z: %f \n", _f.x, _f.y, _f.z);
	vec3 largest = _f + vec3(length, depth, width);
	printf("largest values are x: %f y: %f z: %f \n", largest.x, largest.y, largest.z);

    //f
    vertices[0]=new Vertex(_f, fC);
    vertices[0] -> setNormal(glm::normalize(vec3(-1, -1, -1)));

    //e
    vertices[1]=new Vertex(_f+vec3(length, 0.0, 0.0), fC);
    vertices[1] -> setNormal(glm::normalize(vec3(1, -1, -1)));

    //g
    vertices[2]=new Vertex(_f+vec3(length, depth, 0.0), fC);
    vertices[2] -> setNormal(glm::normalize(vec3(1, 1, -1)));

    //h
    vertices[3]=new Vertex(_f+vec3(0.0, depth, 0.0), fC);
    vertices[3] -> setNormal(glm::normalize(vec3(-1, 1, -1)));

    //b
    vertices[4]=new Vertex(_f+vec3(0.0, 0.0, width), fC);
    vertices[4] -> setNormal(glm::normalize(vec3(-1, -1, 1)));

    //c
    vertices[5]=new Vertex(_f+vec3(length, 0.0, width), fC);
    vertices[5] -> setNormal(glm::normalize(vec3(1, -1, 1)));

    //d
    vertices[6]=new Vertex(_f+vec3(length, depth, width), fC);
    vertices[6] -> setNormal(glm::normalize(vec3(1, 1, 1)));

    //a
    vertices[7]=new Vertex(_f+vec3(0.0, depth, width), fC);
    vertices[7] -> setNormal(glm::normalize(vec3(-1, 1, 1)));


    //f, e, g, h, b, c, d, a
    //0  1  2  3  4  5  6  7
    ein=new Quad1(vertices[3], vertices[0], vertices[4], vertices[7], texture1, vec3(-1.0, 0.0, 0.0)); //good this face faces you it's a face face off!

    zwei=new Quad1(vertices[7], vertices[4], vertices[5], vertices[6], texture1, vec3(0.0, 0.0, -1.0)); //good

    drei=new Quad1(vertices[6], vertices[5], vertices[1], vertices[2], texture1, vec3(1.0, 0.0, 0.0)); //good

    vier=new Quad1(vertices[2], vertices[1], vertices[0], vertices[3], texture1, vec3(0.0, 0.0, 1.0)); //good

    fuenf=new Quad1(vertices[2], vertices[3], vertices[7], vertices[6], texture1, vec3(0.0, 1.0, 0.0)); //top of the rectangular prisim,

    sechs=new Quad1(vertices[0], vertices[4], vertices[5], vertices[1], texture1, vec3(0.0, -1.0, 0.0));

    float calcCenX=0;
    float calcCentY=0;
    float calcCentZ=0;
    //calculate whtere teh center of the card is
    for(int v=0; v<6; v++)
    {
	calcCenX+=vertices[v]->getPos().x;
	calcCentY+=vertices[v]->getPos().y;
	calcCentZ+=vertices[v]->getPos().z;
    }
    calcCenX/=6;
    calcCentY/=6;
    calcCentZ/=6;

    center=vec3(calcCenX, calcCentY, calcCentZ);
}

Rectangular_Prisim2::Rectangular_Prisim2(vec3 _f, double _length, double _width, double _depth, vec3 fC, vec3 bC, Texture* tex1, Texture* tex2)
{
    fillColor=fC;
    borderColor=bC;
    length=_length;
    width=_width;
    depth=_depth;
    texture1=tex1;
    texture2=tex2;
    anchorPoint = _f;
    //f, e, g, h, b, c, d, a
    
    boundingbox = new BoundingBox(_f, length, width, depth);

	printf("base vertex is x: %f y: %f z: %f \n", _f.x, _f.y, _f.z);
	vec3 largest = _f + vec3(length, depth, width);
	printf("largest values are x: %f y: %f z: %f \n", largest.x, largest.y, largest.z);
    
    
    //f
    vertices[0]=new Vertex(_f, fC);
    vertices[0] -> setNormal(glm::normalize(vec3(-1, -1, -1)));
    
    //e
    vertices[1]=new Vertex(_f+vec3(length, 0.0, 0.0), fC);
    vertices[1] -> setNormal(glm::normalize(vec3(1, -1, -1)));
    
    //g
    vertices[2]=new Vertex(_f+vec3(length, depth, 0.0), fC);
    vertices[2] -> setNormal(glm::normalize(vec3(1, 1, -1)));
    
    //h
    vertices[3]=new Vertex(_f+vec3(0.0, depth, 0.0), fC);
    vertices[3] -> setNormal(glm::normalize(vec3(-1, 1, -1)));
    
    //b
    vertices[4]=new Vertex(_f+vec3(0.0, 0.0, width), fC);
    vertices[4] -> setNormal(glm::normalize(vec3(-1, -1, 1)));
    
    //c
    vertices[5]=new Vertex(_f+vec3(length, 0.0, width), fC);
    vertices[5] -> setNormal(glm::normalize(vec3(1, -1, 1)));
    
    //d
    vertices[6]=new Vertex(_f+vec3(length, depth, width), fC);
    vertices[6] -> setNormal(glm::normalize(vec3(1, 1, 1)));
    
    //a
    vertices[7]=new Vertex(_f+vec3(0.0, depth, width), fC);
    vertices[7] -> setNormal(glm::normalize(vec3(-1, 1, 1)));
    
    
    //f, e, g, h, b, c, d, a
    //0  1  2  3  4  5  6  7
    ein=new Quad1(vertices[3], vertices[0], vertices[4], vertices[7], tex1); //good this face faces you it's a face face off!
    
    zwei=new Quad1(vertices[7], vertices[4], vertices[5], vertices[6], tex2); //good
    
    drei=new Quad1(vertices[6], vertices[5], vertices[1], vertices[2], tex1); //good
    
    vier=new Quad1(vertices[2], vertices[1], vertices[0], vertices[3], tex2); //good
    
    fuenf=new Quad1(vertices[2], vertices[3], vertices[7], vertices[6], tex1); //top of the rectangular prisim,
    
    sechs=new Quad1(vertices[0], vertices[4], vertices[5], vertices[1], tex2);
    
    float calcCenX=0;
    float calcCentY=0;
    float calcCentZ=0;
    //calculate whtere teh center of the card is
    for(int v=0; v<6; v++)
    {
        calcCenX+=vertices[v]->getPos().x;
        calcCentY+=vertices[v]->getPos().y;
        calcCentZ+=vertices[v]->getPos().z;
    }
    calcCenX/=6;
    calcCentY/=6;
    calcCentZ/=6;
    
    center=vec3(calcCenX, calcCentY, calcCentZ);
}


// Copy constructor is UNDER CONSTRUCTION (get it?!)

Rectangular_Prisim2::Rectangular_Prisim2(const Rectangular_Prisim2& orig)
{
    //still no not really ~ Suzuka
}

Rectangular_Prisim2::~Rectangular_Prisim2()
{
    // Delete triangles
    // Delete vertices
    delete ein;
    delete zwei;
    delete drei;
    delete vier;
    delete fuenf;
    delete sechs;
    delete texture1;
    delete texture2;
}

void Rectangular_Prisim2::set_whatIsSeenCanBeUnseen()
{
    ein->setShouldDraw(false);
    zwei->setShouldDraw(false);
    drei->setShouldDraw(false);
    vier->setShouldDraw(false);
    fuenf->setShouldDraw(false);
    sechs->setShouldDraw(false);
}



void Rectangular_Prisim2::setWhichFacesToBeDrawn(bool elf, bool zwoelf, bool dreizehn, bool vierzehn, bool fuenfzehn, bool sechzehn)
{
    ein->setShouldDraw(elf);
    zwei->setShouldDraw(zwoelf);
    drei->setShouldDraw(dreizehn);
    vier->setShouldDraw(vierzehn);
    fuenf->setShouldDraw(fuenfzehn);
    sechs->setShouldDraw(sechzehn);
}


// The drawing function
// Pre: None
// Post: The Rectangular_Prisim2 is drawn to the screen
void Rectangular_Prisim2::draw(Shader* s)
{
    ein->draw(s, true);
    zwei->draw(s, true);
    drei->draw(s, true);
    vier->draw(s, true);
    fuenf->draw(s, true);
    sechs->draw(s, true);
}
