/* 
 * File:   DrawableObject.h
 * Author: stuetzlec
 *
 * Created on November 10, 2017, 3:37 PM
 */

#ifndef DRAWABLEOBJECT_H
#define DRAWABLEOBJECT_H

#include <glm/glm.hpp>
#include "Shader.hpp"
#include "Camera.h"
#include "BoundingBox.h"

class DrawableObject
{
public:

    DrawableObject()
    {
	model = glm::mat4 (1.0f);
	isIsohedron = false;
	isSphere = false;
	isDestroyed = false;
    }
	bool getisDestroyed()
	{
		return isDestroyed;
	}
	void Destroy()
	{
		isDestroyed = true;
	}
	void Spawn()
	{
		isDestroyed = false;
	}
	bool getisSphere()
	{
		return isSphere;
	}
	bool getIsIsohedron()
	{
		return isIsohedron;
	}
	BoundingBox* getBoundingBox()
	{
		return boundingbox;
	}

    virtual void draw(Shader*) = 0;
    //virtual vec3 getEyeDirection(vec3) = 0;

    glm::mat4 getModelMatrix()
    {
	return model;
    }

    void applyTransformation(glm::mat4 t)
    {
	model = t * model;
    }

    //added by emma

    void setModelMatrix(glm::mat4 _model)
    {
	model = _model;
    }

    //added by emma

    glm::vec3 getCenter()
    {
	return center;
    }
private:
    vec3 center;//aded by emma
protected:
    glm::mat4 model;
	bool isIsohedron;
	bool isSphere;
	bool isDestroyed;
	BoundingBox* boundingbox;

};

#endif /* DRAWABLEOBJECT_H */

