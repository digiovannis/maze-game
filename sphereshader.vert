#version 330 core

layout(location = 0) in vec3 VertexPosition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 MNormal;

out VS_OUT {
    out vec3 color;
	out vec3 vFragPos;
} vs_out;



void main()
{
	vec3 VertexColor = vec3( 1, 0, 0);
	vs_out.color = VertexColor;

	vs_out.vFragPos = vec3(model *vec4(VertexPosition, 1.0));

	gl_Position = projection * view * vec4(vs_out.vFragPos, 1.0);
}
