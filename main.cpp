/*
 * File:   main.cpp
 * Author: Suzuka/Emma Theberge, Shaila DiGiovanni
 *
 * Date: Dec 5, 2017
 *
 * some code copied from:
 *	    https://github.com/SonarSystems/OpenGL-
 *	    Tutorials/blob/master/GLFW%20Mouse%20Input
 *	    /main.cpp
 *
 * Some code taken from and altered from:
 * _CSC5210_Unit4_PerspectiveCameraCode
 * SOme code also taken and repurposed from Unit 7 Lighting
 * Example class
 *
 * for images that were not already PNGs I used
 * converted to PNG using:
 * https://www.online-convert.com/result/b3ee3611-0731-44a4-aa2127105e9b7e9c
 *
 * 
 * retrieved images of Stuetzle's dog from an email from Stuetzle
 * 
 * created texture not found 404 in powerpoint
 */

#include <GL/glew.h>
// User Includes
#define GLM_FORCE_RADIANS       // Necessary because GLM reasons
#include <cstdio>
#include <math.h>
#include <vector>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "light/DirectionalLight.hpp"
#include "light/AmbientLight.hpp"
#include "Rectangular_Prisim2.h"
#include "light/PointLight.h"
#include "light\\SpotLight.h"
#include "OhRatsLabyrinth.h"
#include "GLFWApplication.h"
#include "GLFWApplication.h"
#include "Icosahedron.h"
#include "Texture.hpp"
#include "Shader.hpp"
#include "Sphere.h"
#include "Camera.h"
#include "Quad1.h"


using std::endl;
using std::cerr;

// The current mouse position
double deltaX, deltaY;
bool lButtonDown;
bool rButtonDown;
vec2 mousePos;

float inverseX, inverseY, inverseZ;
float regularX, regularY, regularZ;

bool suzuka=false;//basically is Emma using this?


//serch paths for the thingys
string searchPath = "C:\\Users\\digio\\Documents\\Visual Studio 2015\\Projects\\CSC5210\\Project6\\Project6\\";
//string searchPath="/Volumes/NO NAME/College_2017-2018/CSC 5210 Computer Graphics/Projects/Final project/v 01 Final Project/v 01 Final Project/";

glm::mat4 idenity; //will hold the idenity matrix

GLFWApplication newApplication;

// Window information
GLuint winHeight=1000;
GLuint winWidth=1000;


//Shaders
Shader* mainShader;
Shader* testShader;

// Lights!!!!
Light* ambLight;
Light* dirLight;
Light* spotLight;


//CAMERA!!!!
Camera* camera;

//TEXTURES!!!!
Texture* tableTexture;
Texture* tex404notfound; //404 TEXTURE NOT FOUND (png).png
Texture* dStuetzle1; // Diesel_Stuetzle_20170204.png
Texture* dStuetzle2; //20170313_132722.png
Texture* stoneWallTextures [3];
Texture* energy;
/* stone wall textures:
 * http://www.lughertexture.com/images/textures/details/bricks_1/stone_bricks_6/medieval_messy_stones_wall_5_20131009_1340152524.jpg
 * http://haammss.com/daut/as/m/d/dry-stone-wall-wallpaper-texture_black-and-white-stone-walls_home-decor_home-decor-liquidators-decorating-blogs-inexpensive-traditional-ideas-decorators-collection-fabric-and-nautical_797x405.jpg
 * http://spiralgraphics.biz/packs/terrain_civilization/previews/Gray%20Stone%20Road.jpg
 */



//surface stuff
const float LENGTH = 240; //X
const float WIDTH = 240; //Z
const float DEPTH = 10.0f; //Y
const vec3 SURFACE_POS = vec3(-1 * (LENGTH / 2), -1 * DEPTH, -1 * (WIDTH / 2));

const float X_UPPERBOUND = (LENGTH / 2);
const float X_LOWERRBOUND = -1 * (LENGTH / 2);
const float Z_UPPERBOUND = (WIDTH / 2);
const float Z_LOWERRBOUND = -1 * (WIDTH / 2);

/* STARTING POSITION STUFFFFFS ie position of camera */
vec3 startingPosition = vec3 (5,5,-5);

//MediumWood found on: https://www.opengl.org/discussion_boards/showthread.php/132502-Color-tables
//colors
vec3 white = vec3(1, 1, 1);
vec3 black = vec3(0, 0, 0);
vec3 mediumWood=vec3(0.65, 0.50f, 0.39);
vec3 darkWood=vec3(0.52, 0.37, 0.26);
vec3 seaGreen = vec3(0.137255f, 0.556863f, 0.419608f);

//3D models identifiers
Rectangular_Prisim2* surface;
OhRatsLabyrinth* ratsLabyrinth;
//Icosahedron* orb;
Sphere* orb;


/**
 * init
 * Initalizes:
 * Textures, Shapes/Geometry models, Camera, Shaders, and glEnable(something),
 * also what should be drawn to the screen
 */
void init()
{
	
    //for images that were not already PNGs I used
    //converted to PNG using: https://www.online-convert.com/result/b3ee3611-0731-44a4-aa21-27105e9b7e9c
    //to convert them to be pngs
    //or http://jpg2png.com

    
	tex404notfound = new Texture(searchPath + "404 TEXTURE NOT FOUND (png).png");
	dStuetzle1 = new Texture(searchPath + "Diesel_Stuetzle_20170204.png");
	dStuetzle2 = new Texture(searchPath + "20170313_132722.png");
	energy = new Texture(searchPath + "Heavyplasma.png");

	stoneWallTextures[0] = new Texture(searchPath+"Gray Stone Road.png");
	stoneWallTextures[1] = new Texture(searchPath+"dryStoneWall.png");
	stoneWallTextures[2] = new Texture(searchPath+"medieval_messy_stones_wall_5_20131009_1340152524.png");

	
	surface=new Rectangular_Prisim2(SURFACE_POS, LENGTH, WIDTH, DEPTH, seaGreen, black, stoneWallTextures[0]);
	//OhRatsLabyrinth(Texture* texinnerWallrus1,Texture* texOuterWallrus);
	ratsLabyrinth = new OhRatsLabyrinth(stoneWallTextures[1],stoneWallTextures[2]);
	
	orb = new Sphere();

    //Shader stuff
    string shaders[]=
    {
		searchPath + "verticesmulti.vert", searchPath + "fragmentsmulti.frag"
    };

    mainShader = new Shader(shaders, 2, true);
	
	string shaderstess[] =
	{
		searchPath + "tessshader.vert", searchPath + "tessshader.frag"//, searchPath + "geometry.glsl", searchPath + "tesscontrol.glsl", searchPath + "tesseval.glsl"
	};

	testShader = new Shader(shaderstess, 2, true);
	newApplication.setTessShader(testShader);
	
	string shaderssphere[] =
	{
		searchPath + "sphereshader.vert", searchPath + "sphereshader.frag",  searchPath + "sphereshader.glsl"
	};

	testShader = new Shader(shaderssphere, 3, true);
	newApplication.setsphereShader(testShader);
    
	
    GLfloat FOV=45.0f;
    GLfloat nearPlane=0.1f;
    GLfloat farPlane=1000.0f;
    camera=new Camera(startingPosition, winWidth, winHeight, LENGTH, WIDTH);
    camera -> setPerspective(FOV, (GLfloat) winWidth/(GLfloat) winHeight, nearPlane, farPlane);

    // Finally, tell the application object about the drawable objects
    newApplication.addDrawableObject(surface);

	surface->setWhichFacesToBeDrawn(false, false, false, false, true, false);

    for (int i=0; i<ratsLabyrinth->getsizeInner(); i++)
    {
	newApplication.addDrawableObject(ratsLabyrinth->get_innerWalrus_atIndex(i));
    }
    
    for (int i=0; i<ratsLabyrinth->getsizeOuter(); i++)
    {
	newApplication.addDrawableObject(ratsLabyrinth->get_outerWalrus_atIndex(i));
    }
	//newApplication.addDrawableObject(orb);
    
	DrawableObject* block = new Rectangular_Prisim2(
		vec3(-48, 3, -68), 3, 3, 3, mediumWood, darkWood, energy);
	newApplication.addSmashableObject(block);

	block = new Rectangular_Prisim2(
		vec3(-98, 3, 18), 3, 3, 3, mediumWood, darkWood, energy);
	newApplication.addSmashableObject(block);

	block = new Rectangular_Prisim2(
		vec3(12, 3, 12), 3, 3, 3, mediumWood, darkWood, energy);
	newApplication.addSmashableObject(block);

	block = new Rectangular_Prisim2(
		vec3(-98, 3, -108), 3, 3, 3, mediumWood, darkWood, energy);
	newApplication.addSmashableObject(block);

    // Set up the lights
    ambLight=new AmbientLight(vec3(0.005, 0.005, 0.005));
    ambLight -> enable();

	spotLight = new SpotLight(camera->getPosition(), vec3(1.0f, 1.0f, 1.0f), camera->getFront(), glm::cos(glm::radians(17.5f)), glm::cos(glm::radians(30.0f)), 1.0f, 1.0f, 0.09f, 0.032f);
	spotLight->enable();

	dirLight = new DirectionalLight(vec3(0.01, 0.01, 0.009), glm::normalize(vec3(-1.0, 1.0, 1.0)), 0.8, 0.51);
	dirLight->enable();


	newApplication.addLight(ambLight);
	newApplication.addLight(dirLight);
	newApplication.addLight(spotLight);
	newApplication.setSpotLight((SpotLight*)spotLight);
	spotLight->toggle();



}//init end



/**
 *
 * @param window
 * @param key = N
 * @param scancode
 * @param action
 * @param mode
 */
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if(action==GLFW_PRESS)
    {
		if(key==GLFW_KEY_Q||key==GLFW_KEY_ESCAPE)
		{
			glfwSetWindowShouldClose(window, true);
		}
		if (key == GLFW_KEY_W)
		{
			newApplication.setwalkingforward(true);
		}
		if (key == GLFW_KEY_S)
		{
			newApplication.setwalkingbackward(true);
		}
		if (key == GLFW_KEY_A)
		{
			newApplication.setwalkingleft(true);
		}
		if (key == GLFW_KEY_D)
		{
			newApplication.setwalkingright(true);
		}
		if (key == GLFW_KEY_DOWN)
		{
			newApplication.setheaddown(true);
		}
		if (key == GLFW_KEY_UP)
		{
			newApplication.setheadup(true);
		}
		if (key == GLFW_KEY_LEFT)
		{
			newApplication.setheadleft(true);
		}
		if (key == GLFW_KEY_RIGHT)
		{
			newApplication.setheadright(true);
		}
	if(suzuka)
	{
	    switch (key)
	    {
		case GLFW_KEY_3: //move player down
		    {
				camera->moveCamera(0,50.0f);
		    }
		    break;
		case GLFW_KEY_4: //move player up
		    {
				camera->moveCamera(0,-5.0f);
		    }
		    break;
		}
	}
		
    }
    else if(action==GLFW_RELEASE)
    {
		if (key == GLFW_KEY_LEFT_SHIFT)
		{
			ratsLabyrinth->set_WhatisSEENcanBE_UNseEn();
		}
		if (key == GLFW_KEY_RIGHT_SHIFT)
		{
			ratsLabyrinth->setDefaultDraw();
		}
		if(key==GLFW_KEY_P) 
		{
			if (newApplication.getWin())
			{
				camera->setPosition(startingPosition);
				DrawableObject* block = new Rectangular_Prisim2(
					vec3(20, 3, -5), (double)3, (double)3, (double)3, mediumWood, darkWood, dStuetzle2);
				newApplication.addDrawableObject(block);
		
				//printf("placing block\n");
			}
		}
		if (key == GLFW_KEY_SPACE)
		{
			newApplication.smash();
		}
		if(key==GLFW_KEY_T)
		{
			dirLight -> toggle();
		}
		if (key == GLFW_KEY_F)
		{
			spotLight->toggle();
		}
		if (key == GLFW_KEY_W)
		{
			newApplication.setwalkingforward(false);
		}
		if (key == GLFW_KEY_S)
		{
			newApplication.setwalkingbackward(false);
		}
		if (key == GLFW_KEY_A)
		{
			newApplication.setwalkingleft(false);
		}
		if (key == GLFW_KEY_D)
		{
			newApplication.setwalkingright(false);
		}
		if (key == GLFW_KEY_DOWN)
		{
			newApplication.setheaddown(false);
		}
		if (key == GLFW_KEY_UP)
		{
			newApplication.setheadup(false);
		}
		if (key == GLFW_KEY_LEFT)
		{
			newApplication.setheadleft(false);
		}
		if (key == GLFW_KEY_RIGHT)
		{
			newApplication.setheadright(false);
		}
		if (key == GLFW_KEY_7)
		{
			camera->setPosition(vec3(-45, 5, -55));
		}
		if (key == GLFW_KEY_8)
		{
			camera->setPosition(vec3(-95, 5, -100));
		}
		if (key == GLFW_KEY_9)
		{
			camera->setPosition(vec3(-95, 5, 5));
		}
		if (suzuka)
        {
            switch (key)
            {
                case GLFW_KEY_1:
                    {
                        //print out current camera position and yaw and pitch
                        //camera->printAllInfo();
                    }
                    break;
			  case GLFW_KEY_LEFT_SHIFT: //change the textures of the surface, outer walls and inner walls
			  {
				  //ratsLabyrinth->set_WhatisSEENcanBE_UNseEn();
			  }
			  break;
            }
        }
		
	
    }
}

// When a mouse button is clicked, change the state of the "left" and "right"
//   buttons to being held down (when pressed) or not (when released).
void mouse_callback(GLFWwindow* window, int button, int action, int mods)
{
    if(button==GLFW_MOUSE_BUTTON_LEFT)
    {
        if(GLFW_PRESS==action)
            lButtonDown=true;
        
        else if(GLFW_RELEASE==action)
            lButtonDown=false;
    }
}

// Update the mouse position (and the change in the position from the
//   previous update) in here.
void mouse_motion_callback(GLFWwindow* window, double mX, double mY)
{
    deltaX=mousePos.x-mX;
    deltaY=mousePos.y-mY;
    mousePos.x=mX;
    mousePos.y=mY;
    
    // If the associated button is down, make sure to update the camera accordingly.
    if(lButtonDown)
    {
        camera -> setViewByMouse(deltaX, deltaY);
    }
}

/*
 * glfw: whenever the window size changed (by OS or user resize)
 * this callback function executes
 */

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    /*
     * make sure the viewport matches the new window dimensions;
     * note that width and
     * height will be significantly larger than specified on
     * retina displays.
     */
    glViewport(0, 0, width, height);
}

/**
 * taken from lighting example unit 7
 */
int main()
{
    // Set up the GLFW application
    //anti aliasing level
    //minor version (Opengl)
    //major version (Opengl)
    //window title
    //window width
    //window height
    newApplication.initializeApplication(8, 1, 4,
					 "Final Project Maze Game",
					 winWidth, winHeight);

    // Assign your callback functions (the ones you write) to the internal
    //   callbacks of the application class.

    newApplication.setKeyCallback(key_callback);
    newApplication.setMouseClickCallback(mouse_callback);
    newApplication.setMouseMotionCallback(mouse_motion_callback);

    newApplication.initializeCallbacks();
    // Initialize stuff local to the program
    init();
    newApplication.setShader(mainShader);
    newApplication.setCamera(camera);

    // Tell the application to "go"
    newApplication.initiateDrawLoop();

    return 0;
}
