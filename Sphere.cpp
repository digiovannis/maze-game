#include "Sphere.h"



Sphere::Sphere()
{
	isSphere = true;
	//triangle 1 Triangles[0. 0. 1.][1. 0. 0.][0. 1. 0.]
	verts[0] = 0;
	verts[1] = 0;
	verts[2] = 1;

	verts[3] = 1;
	verts[4] = 0;
	verts[5] = 0;

	verts[6] = 0;
	verts[7] = 1;
	verts[8] = 0;
	//triangle 2 Triangles[1. 0. 0.][0. 0. - 1.][0. 1. 0.]
	verts[9] = 1;
	verts[10] = 0;
	verts[11] = 0;

	verts[12] = 0;
	verts[13] = 0;
	verts[14] = -1;

	verts[15] = 0;
	verts[16] = 1;
	verts[17] = 0;
	//triangle 3 Triangles[0. 0. - 1.][-1. 0. 0.][0. 1. 0.]
	verts[18] = 0;
	verts[19] = 0;
	verts[20] = -1;

	verts[21] = -1;
	verts[22] = 0;
	verts[23] = 0;

	verts[24] = 0;
	verts[25] = 1;
	verts[26] = 0;
	//triangle 4 Triangles[-1. 0. 0.][0. 0. 1.][0. 1. 0.]
	verts[27] = -1;
	verts[28] = 0;
	verts[29] = 0;

	verts[30] = 0;
	verts[31] = 0;
	verts[32] = 1;

	verts[33] = 0;
	verts[34] = 1;
	verts[35] = 0;
	//triangle 5 Triangles[0. 0. 1.][1. 0. 0.][0. - 1. 0.]
	verts[36] = 0;
	verts[37] = 0;
	verts[38] = 1;

	verts[39] = 1;
	verts[40] = 0;
	verts[41] = 0;

	verts[42] = 0;
	verts[43] = -1;
	verts[44] = 0;
	//triangle 6 Triangles[1. 0. 0.][0. 0. - 1.][0. - 1. 0.]
	verts[45] = 1;
	verts[46] = 0;
	verts[47] = 0;

	verts[48] = 0;
	verts[49] = 0;
	verts[50] = -1;

	verts[51] = 0;
	verts[52] = -1;
	verts[53] = 0;
	//triangle 7 Triangles[0. 0. - 1.][-1. 0. 0.][0. - 1. 0.] 
	verts[54] = 0;
	verts[55] = 0;
	verts[56] = -1;

	verts[57] = -1;
	verts[58] = 0;
	verts[59] = 0;

	verts[60] = 0;
	verts[61] = -1;
	verts[62] = 0;
	//triangle 8 Triangles[-1. 0. 0.][0. 0. 1.][0. - 1. 0.]
	verts[63] = -1;
	verts[64] = 0;
	verts[65] = 0;

	verts[66] = 0;
	verts[67] = 0;
	verts[68] = 1;

	verts[69] = 0;
	verts[70] = -1;
	verts[71] = 0;
	
	//set up the vertex array obj and vertex buff
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// Populate with the data
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), &verts, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
		
		
}
void Sphere::draw(Shader* s)
{
	s->useProgram();
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 72);
}

Sphere::~Sphere()
{
}
