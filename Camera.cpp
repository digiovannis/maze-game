/* Code taken from and altered from:
 * _CSC5210_Unit7_ Lighting
 * (Class Example)
 *
 * FINAL PROJECT
 * Shaila DiGiovanni & Emma Theberge
 */

#include <time.h>
#include "Camera.h"

Camera::Camera()
{
    assert(false&&"Do not use the default Camera constructor.");
}

Camera::Camera(vec3 pos, float width, float height, float surfLength, float surfWidth)
{
    Position=pos;
    ScreenWidth=width;
    ScreenHeight=height;

	verticalheight = Position.y;

	boundingbox = new BoundingBox(Position - vec3(2, verticalheight-1, 2), 4, 4, verticalheight);

	xupper = (surfLength / 2);
	xlower = -1 * (surfLength / 2);
	zupper = (surfWidth / 2);
	zlower = -1 * (surfWidth / 2);

    setDefaults();
	updateCameraVectors();
}

void Camera::setDefaults()
{
    FOV=45.0f;
    AspectRatio=(float) ScreenWidth/(float) ScreenHeight;
    nearPlane=0.1f;
    farPlane=500.0f;
	WorldUp = vec3(0.0f, 1.0f, 0.0f);
	Yaw = -90.0f;
    Pitch=0.0; // Look directly down the z-axis to start
    MouseSpeed=0.1f;
    Speed=10.f;
	MovementSpeed = 15.0f;
    RotationSpeed=0.01f;
}

void Camera::updateCameraVectors()
{
	// Calculate the new Front vector
	glm::vec3 front;
	front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	front.y = sin(glm::radians(Pitch));
	front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	Front = glm::normalize(front);
	// Also re-calculate the Right and Up vector
	Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	Up = glm::normalize(glm::cross(Right, Front));
}


// This function returns the View matrix, based on the position, up vector,
//   and look-at position.
// Pre:
// Post:

mat4 Camera::getViewMatrix()
{
    //glm::vec3 look=Position+getViewVector();
    //glm::vec3 up=calculateUpVector();

	return lookAt(Position, Position + Front, Up);
}

// This function returns the projection matrix, based on the camera's position,
//   and the field of view and aspect ratios, along with distances between
//   near and far planes
// Pre:
// Post:

mat4 Camera::getPerspectiveMatrix()
{
    return glm::perspective(FOV, AspectRatio, nearPlane, farPlane);
}


// This function returns the Projection Matrix of an orthographic projection
// Pre:
// Post:

mat4 Camera::getOrthographicMatrix()
{
	printf("no such thing implemented yet\n");
	return glm::mat4 (1);
}

// This function sets the persepctive parameters for the persepctive camera
// Pre:
// Post:

void Camera::setPerspective(float _fov, float ar, float np, float fp)
{
    FOV=_fov;
    AspectRatio=ar;
    nearPlane=np;
    farPlane=fp;

	updateCameraVectors();
    
}

// This function sets the position to the argument _pos
// Pre:
// Post:

void Camera::setPosition(vec3 _pos)
{
    Position=_pos;
}

// This function sets the position to the argument _pos
// Pre:
// Post:

void Camera::setPosition(float x, float y, float z)
{
    Position=vec3(x, y, z);
}

// Return the current position of the camera
// Pre:
// Post:

vec3 Camera::getPosition()
{
    return Position;
}

vec3 Camera::getFront()
{
	return Front;
}

// Return the current mouse speed
// Pre:
// Post:

float Camera::getMouseSpeed()
{
    return MouseSpeed;
}

// This function updates the local deltaTime variable
// Pre:
// Post:

void Camera::setDeltaTime(double dt)
{
    deltaTime=dt;
}

/************************
 ** MOVEMENT FUNCTIONS **
 ***********************/

// This function moves the camera based on the current mouse position, based on
//   dX and dY which are movements in the mouse position
// Pre:
// Post:

void Camera::setViewByMouse(float dX, float dY)
{
    Yaw+=MouseSpeed*deltaTime * dX;
    Pitch+=MouseSpeed*deltaTime * dY;

	updateCameraVectors();
}

// This function adjusts the camera position by dX and dY
// Pre:
// Post:

void Camera::moveCamera(float dX, float dY)
{
    Position+=glm::vec3(dX*deltaTime * Speed, dY*deltaTime * Speed, 0);

	updateCameraVectors();
}

vec3 Camera::getPos3BlocksInfront()
{
	vec3 tempPos = Position;
	float velocity = 15.0;
	printf("velocity = %f\n", velocity);
	tempPos += Front * velocity;
	return tempPos;
}

void Camera::updateBB()
{
	boundingbox->setPosition(Position - vec3(2, verticalheight -1, 2));
}

/***********************************************************************/
//edit to also pass in smashables, separate smashable objects from drawables so that they do not overlapp
void Camera::ProcessMovement(Camera_Movement direction, vector< DrawableObject* > &drawables, vector< DrawableObject* > &smashables)
{
	vec3 previousPos = Position;

	vec3 temppos = Position;

	vec3 moveby = vec3(0,0,0);

	float velocity = MovementSpeed * deltaTime;
	if (direction == FORWARD)
		moveby += Front * velocity;
	if (direction == BACKWARD)
		moveby -= Front * velocity;
	if (direction == LEFT)
		moveby -= Right * velocity;
	if (direction == RIGHT)
		moveby += Right * velocity;


	temppos += moveby;
	

	int i, j;
	bool collision = false;
	for (i = 0; i < drawables.size() && !collision; i++)
	{
		
		if (!(drawables[i]->getisDestroyed()));
			collision = CheckCollision(temppos, drawables[i]);
		
	}
	i--;
	for (j = 0; j < smashables.size() && !collision; j++)
	{
		
		if (!(smashables[j]->getisDestroyed()));
			collision = CheckCollision(temppos, smashables[j]);

	}
	j--;
	if (collision)
	{
		//printf("colliding with drawable object %d or smashable object %d\n", i, j);
		
		updateBB();
		collision = false;
	}
	else
	{
		Position = vec3(temppos.x, verticalheight, temppos.z);
		updateBB();
	}

	
	
	
}

void Camera::ProcessSmash(vector< DrawableObject* > &smashables)
{

	vec3 temppos = Position;
	vec3 moveby = vec3(0, 0, 0);
	float velocity = 7.0;//MovementSpeed * deltaTime;
	moveby += Front * velocity;
	temppos += moveby;


	int i;
	bool collision = false;
	for (i = 0; i < smashables.size() && !collision; i++)
	{
		collision = CheckSmash(temppos, smashables[i]);

	}
	i--;
	if (collision)
	{
		
		smashables.erase(smashables.begin() + i);

		collision = false;
	}
	
	
}

//returns true if collison is detected
GLboolean Camera::CheckSmash(vec3 pos, DrawableObject* object) // AABB - AABB collision
{
	BoundingBox* objectbox = object->getBoundingBox();
	vec3 objectpos = objectbox->getPosition();
	vec3 smashpos = pos - vec3(1, 1, 1);
	// Collision x-axis?
	bool collisionX = smashpos.x + 2 >= objectpos.x && objectpos.x + objectbox->getLength() >= smashpos.x;

	// Collision y-axis?
	bool collisionY = smashpos.y + 2 >= objectpos.y && objectpos.y + objectbox->getDepth() >= smashpos.y;

	//Collision z-axis?
	bool collisionZ = smashpos.z + 2 >= objectpos.z && objectpos.z + objectbox->getWidth() >= smashpos.z;

	// Collision only if on all axes
	return collisionX && collisionY && collisionZ;
}

//returns true if collison is detected
GLboolean Camera::CheckCollision(vec3 pos, DrawableObject* object) // AABB - AABB collision
{
	BoundingBox* objectbox = object->getBoundingBox();
	vec3 objectpos = objectbox->getPosition();
	vec3 playerpos = pos - vec3(2, verticalheight - 1, 2);
	// Collision x-axis?
	bool collisionX = playerpos.x + boundingbox->getLength() >= objectpos.x && objectpos.x + objectbox->getLength() >= playerpos.x;
		
	// Collision y-axis?
	bool collisionY = playerpos.y + boundingbox->getDepth() >= objectpos.y && objectpos.y + objectbox->getDepth() >= playerpos.y;
		
	//Collision z-axis?
	bool collisionZ = playerpos.z + boundingbox->getWidth() >= objectpos.z && objectpos.z + objectbox->getWidth() >= playerpos.z;

	// Collision only if on all axes
	return collisionX && collisionY && collisionZ;
}

void Camera::ProcessHeadMovement(float xoffset, float yoffset, GLboolean constrainPitch = true)
{
	
	Yaw += xoffset;
	Pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch)
	{
		if (Pitch > 89.0f)
			Pitch = 89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;
	}

	// Update Front, Right and Up Vectors using the updated Eular angles
	updateCameraVectors();
}


// This function zooms the camera based on the number of mouse wheel clicks that
//   have been moved.
// Pre:
// Post:

void Camera::zoomCamera(float delta)
{
    Position+=vec3(0, 0, delta*deltaTime*RotationSpeed);

	updateCameraVectors();
}


// This function calculates and returns the view direction
// Pre:
// Post:

glm::vec3 Camera::getViewVector()
{
    glm::vec3 direction(
			cos(Pitch)*sin(Yaw),
			sin(Pitch),
			cos(Pitch)*cos(Yaw)
			);

    direction=glm::normalize(direction);
    return direction;
}

// This function returns the vector looking directly to the right of the camera
//   Since we're not changing the vertical component when moving
//   (we're not ever rotating the 'roll' component), Y is 0
// Pre:
// Post:

glm::vec3 Camera::getRightVector()
{
    glm::vec3 right=glm::vec3(
			      sin(Yaw-PI/2.0f),
			      0,
			      cos(Yaw-PI/2.0f)
			      );
    right=glm::normalize(right);
    return right;
}

void Camera::printOutYawandPitch()
{
    printf("Yaw: %f\n",Yaw);
    printf("Pitch: %f\n",Pitch);
}


// This function calculates the current 'up' vector based on the front and
//   right vectors
// Pre: front and right are correct
// Post:

glm::vec3 Camera::calculateUpVector()
{
    return glm::normalize(glm::cross(getRightVector(), getViewVector()));
}
