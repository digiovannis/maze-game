#include "BoundingBox.h"



BoundingBox::BoundingBox(vec3 pos, float _length, float _width, float _depth)
{
	position = pos;
	length = _length;
	width = _width;
	depth = _depth;
}


BoundingBox::~BoundingBox()
{
}

float BoundingBox::getLength()
{
	return length;
}

float BoundingBox::getWidth()
{
	return width;
}

float BoundingBox::getDepth()
{
	return depth;
}

vec3 BoundingBox::getPosition()
{
	return position;
}

void BoundingBox::setPosition(vec3 pos)
{
	position = pos;
}
