/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OhRatsLabyrinth.h
 * Author: Suzuka
 *
 * Date: December 10, 2017, 12:59 PM
 */

#ifndef OHRATSLABYRINTH_H
#define OHRATSLABYRINTH_H

#include "Rectangular_Prisim2.h"
#include "DrawableObject.h"
#include "Texture.hpp"

class OhRatsLabyrinth
{
public:
    OhRatsLabyrinth(Texture* texinnerWallrus1,Texture* texOuterWallrus);
    
    Rectangular_Prisim2* get_innerWalrus_atIndex(int index);
    
    Rectangular_Prisim2* get_outerWalrus_atIndex(int index);
    
    int getsizeInner()
    {
    	return sizeOfInnerwalrus;
    }
    
    int getsizeOuter()
    {
    	return sizeOfOuterwalrus;
    }
    
    OhRatsLabyrinth(const OhRatsLabyrinth& orig);
    
    void setDefaultDraw();
    
    void set_innerWalrusTexture(Texture* tex);
    void set_innerWalrusTextures(Texture* tex1, Texture* tex2);
    
    void set_outerWalrusTexture(Texture* tex);
    
    void set_WhatisSEENcanBE_UNseEn();
	
    void removeOuterWalls()
    {
    	for (int i=0; i<getsizeOuter(); i++)
		{
	    	outerWalrus[i]->set_whatIsSeenCanBeUnseen();
		}
	}
    
    
    
    virtual ~OhRatsLabyrinth();
private:

    const float groundHog_Y_value = 0;
    const float wallDepth = 10;
    const float wallThickness = 10;//thickness is either X or Z value
    
    vec3 seaGreen=vec3(0.137255f, 0.556863f, 0.419608f);
    
    int sizeOfInnerwalrus=61;
    int sizeOfOuterwalrus=4;
    
    Rectangular_Prisim2* innerWalrus[61];
    Rectangular_Prisim2* outerWalrus[4];
    
    
    
};

#endif /* OHRATSLABYRINTH_H */

