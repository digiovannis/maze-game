/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OhRatsLabyrinth.cpp
 * Author: Suzuka
 * 
 * Date: December 10, 2017, 12:59 PM
 */

#include "OhRatsLabyrinth.h"

OhRatsLabyrinth::OhRatsLabyrinth(Texture* texinnerWallrus1,Texture* texOuterWallrus)
{

innerWalrus[0]= new Rectangular_Prisim2(vec3(0,0,10),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[1]= new Rectangular_Prisim2(vec3(10,0,20),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[2]= new Rectangular_Prisim2(vec3(-20,0,0),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[3]= new Rectangular_Prisim2(vec3(40,0,10),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[4]= new Rectangular_Prisim2(vec3(40,0,-10),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[5]= new Rectangular_Prisim2(vec3(30,0,-20),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[6]= new Rectangular_Prisim2(vec3(0,0,40),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[7]= new Rectangular_Prisim2(vec3(-30,0,60),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[8]= new Rectangular_Prisim2(vec3(-50,0,50),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[9]= new Rectangular_Prisim2(vec3(-70,0,60),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[10]= new Rectangular_Prisim2(vec3(10,0,60),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[11]= new Rectangular_Prisim2(vec3(-110,0,100),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[12]= new Rectangular_Prisim2(vec3(100,0,50),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[13]= new Rectangular_Prisim2(vec3(70,0,30),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[14]= new Rectangular_Prisim2(vec3(100,0,-40),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[15]= new Rectangular_Prisim2(vec3(-90,0,10),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[16]= new Rectangular_Prisim2(vec3(-70,0,0),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[17]= new Rectangular_Prisim2(vec3(-60,0,-70),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[18]= new Rectangular_Prisim2(vec3(50,0,-30),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[19]= new Rectangular_Prisim2(vec3(70,0,-40),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[20]= new Rectangular_Prisim2(vec3(90,0,20),wallThickness,wallThickness*1,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[21]= new Rectangular_Prisim2(vec3(10,0,0), wallThickness*2, wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[22]= new Rectangular_Prisim2(vec3(-50,0,10),wallThickness*2, wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[23]= new Rectangular_Prisim2(vec3(-50,0,-50),wallThickness*2, wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[24]= new Rectangular_Prisim2(vec3(30,0,20),wallThickness*2, wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[25]= new Rectangular_Prisim2(vec3(50,0,-50),wallThickness*2, wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);


innerWalrus[26]= new Rectangular_Prisim2(vec3(-110,0,40),wallThickness,wallThickness*2,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[27]= new Rectangular_Prisim2(vec3(-100,0,70),wallThickness,wallThickness*2,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[28]= new Rectangular_Prisim2(vec3(60,0,-20),wallThickness,wallThickness*2,wallDepth,seaGreen,seaGreen,texOuterWallrus);
innerWalrus[29]= new Rectangular_Prisim2(vec3(90,0,-20),wallThickness,wallThickness*2,wallDepth,seaGreen,seaGreen,texOuterWallrus);

	
	innerWalrus[30]= new Rectangular_Prisim2(vec3(-40,0,-70),wallThickness,wallThickness*2,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[31]= new Rectangular_Prisim2(vec3(70,0,10),wallThickness*3,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[32]= new Rectangular_Prisim2(vec3(-60,0,-80),wallThickness*3,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[33]= new Rectangular_Prisim2(vec3(60,0,10),wallThickness,wallThickness*3,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[34]= new Rectangular_Prisim2(vec3(-20,0,50),wallThickness,wallThickness*3,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[35]= new Rectangular_Prisim2(vec3(60,0,-60),wallThickness*4,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[36]= new Rectangular_Prisim2(vec3(10,0,80),wallThickness*3,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[37]= new Rectangular_Prisim2(vec3(80,0,-40),wallThickness,wallThickness*4,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[38]= new Rectangular_Prisim2(vec3(40,0,-70),wallThickness,wallThickness*4,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[39]= new Rectangular_Prisim2(vec3(-70,0,70),wallThickness*5,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[40]= new Rectangular_Prisim2(vec3(-70,0,-80),wallThickness,wallThickness*5,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[41]= new Rectangular_Prisim2(vec3(20,0,20),wallThickness,wallThickness*5,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[42]= new Rectangular_Prisim2(vec3(40,0,40),wallThickness*6,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[43]= new Rectangular_Prisim2(vec3(40,0,-80),wallThickness*6,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[44]= new Rectangular_Prisim2(vec3(-70,0,-30),wallThickness*6,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[45]= new Rectangular_Prisim2(vec3(50,0,80),wallThickness*6,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[46]= new Rectangular_Prisim2(vec3(-90,0,40),wallThickness,wallThickness*6,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[47]= new Rectangular_Prisim2(vec3(-20,0,-90),wallThickness,wallThickness*6,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[48]= new Rectangular_Prisim2(vec3(-10,0,30),wallThickness,wallThickness*5,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[49]= new Rectangular_Prisim2(vec3(30,0,60),wallThickness*7,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[50]= new Rectangular_Prisim2(vec3(30,0,-100),wallThickness*7,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[51]= new Rectangular_Prisim2(vec3(-80,0,-100),wallThickness*7,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[52]= new Rectangular_Prisim2(vec3(40,0,100),wallThickness*7,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[53]= new Rectangular_Prisim2(vec3(-80,0,40),wallThickness*7,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[54]= new Rectangular_Prisim2(vec3(-90,0,-10),wallThickness*9,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[55]= new Rectangular_Prisim2(vec3(20,0,-100),wallThickness,wallThickness*9,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[56]= new Rectangular_Prisim2(vec3(-90,0,-100),wallThickness,wallThickness*9,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[57]= new Rectangular_Prisim2(vec3(-110,0,20),wallThickness*10,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[58]= new Rectangular_Prisim2(vec3(0,0,-110), wallThickness,wallThickness*10,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[59]= new Rectangular_Prisim2(vec3(-80,0,90),wallThickness*11,wallThickness,wallDepth,seaGreen,seaGreen,texinnerWallrus1);
innerWalrus[60]= new Rectangular_Prisim2(vec3(-110,0,-110),wallThickness,wallThickness*13,wallDepth,seaGreen,seaGreen,texinnerWallrus1);


    outerWalrus[0]=new Rectangular_Prisim2(vec3(-110,0,-120),22*wallThickness,wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);
    outerWalrus[1]=new Rectangular_Prisim2(vec3(110,0,-110),wallThickness,wallThickness*22,wallDepth,seaGreen,seaGreen,texOuterWallrus);
    outerWalrus[2]=new Rectangular_Prisim2(vec3(-110,0,110),22*wallThickness,wallThickness,wallDepth,seaGreen,seaGreen,texOuterWallrus);
    outerWalrus[3]=new Rectangular_Prisim2(vec3(-120,0,-110),wallThickness,wallThickness*22,wallDepth,seaGreen,seaGreen,texOuterWallrus);

}

void OhRatsLabyrinth::set_WhatisSEENcanBE_UNseEn()
{
	for (int i=0; i<getsizeInner(); i++)
	{
	    innerWalrus[i]->set_whatIsSeenCanBeUnseen();
	}
	
	for (int i=0; i<getsizeOuter(); i++)
	{
	    outerWalrus[i]->set_whatIsSeenCanBeUnseen();
	}
}

void OhRatsLabyrinth::setDefaultDraw()
{
	bool t= true;
	bool f=false;
	
    innerWalrus[0]->setWhichFacesToBeDrawn(t , t , t , t , t , f);
    innerWalrus[1]->setWhichFacesToBeDrawn(t , t , f , t , t , f);
    innerWalrus[2]->setWhichFacesToBeDrawn(t , t , t , f , t , f);
    innerWalrus[3]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[4]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[5]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[6]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[7]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[8]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[9]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[10]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[11]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[12]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[13]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[14]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[15]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[16]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[17]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[18]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[19]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[20]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[21]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[22]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[23]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[24]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[25]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[26]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[27]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[28]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[29]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[30]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[31]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[32]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[33]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[34]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[35]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[36]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[37]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[38]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[39]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[40]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[41]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[42]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[43]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[44]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[45]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[46]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[47]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[48]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[49]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[50]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[51]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[52]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[53]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[54]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[55]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[56]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[57]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[58]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[59]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    innerWalrus[60]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);


    outerWalrus[0]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    outerWalrus[1]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    outerWalrus[2]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);
    outerWalrus[3]->setWhichFacesToBeDrawn(t ,  t ,  t ,  t ,  t , f);


}


Rectangular_Prisim2* OhRatsLabyrinth::get_innerWalrus_atIndex(int index)
{
	return innerWalrus[index];
}

Rectangular_Prisim2* OhRatsLabyrinth::get_outerWalrus_atIndex(int index)
{
	return outerWalrus[index];
}

OhRatsLabyrinth::OhRatsLabyrinth(const OhRatsLabyrinth& orig){ }

OhRatsLabyrinth::~OhRatsLabyrinth(){ }

