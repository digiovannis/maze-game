/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AmbientLight.cpp
 * Author: stuetzlec
 * 
 * Created on November 27, 2017, 1:30 PM
 */

#include "AmbientLight.hpp"

AmbientLight::AmbientLight(vec3 col) : Light(col) {

}

AmbientLight::AmbientLight(const AmbientLight& orig) : Light(orig) {

}

AmbientLight::~AmbientLight() {
}

void AmbientLight::connectLightToShader(Shader* s) {
    GLuint aL = s->GetVariable("ambientColor");
    s->SetVector3(aL, 1, &color[0]);

    GLint weight = s->GetVariable("ambientWeight");
    if (isEnabled())
        s->SetFloat(weight, 1.0);
    else
        s->SetFloat(weight, -1.0);
}