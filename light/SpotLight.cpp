#include "SpotLight.h"


SpotLight::SpotLight(vec3 pos, vec3 col, vec3 dir, GLfloat incut, GLfloat outcut, GLfloat sh, GLfloat constantatt, GLfloat linearatt, GLfloat quadraticatt):Light(col)
{
	direction = glm::normalize(dir);
	position = pos;
	shininess = sh;
	cutOff = incut;
	outerCutOff = outcut;

	constant = constantatt;
	linear = linearatt;
	quadratic = quadraticatt;
}

void SpotLight::connectLightToShader(Shader* s) {
	// Connect to the variables
	GLint spotLightid = s->GetVariable("spotLight.color");
	GLint lightDirid = s->GetVariable("spotLight.direction");
	GLint shininessid = s->GetVariable("spotLight.Shininess");
	GLint spotPosid = s->GetVariable("spotLight.position");
	GLint spotInCutid = s->GetVariable("spotLight.cutOff");
	GLint spotOutCutid = s->GetVariable("spotLight.outerCutOff");
	GLint conAttenID, linAttenID, quadAttenID;
	conAttenID = s->GetVariable("spotLight.constant");
	linAttenID = s->GetVariable("spotLight.linear");
	quadAttenID = s->GetVariable("spotLight.quadratic");
	GLint spotLightEnable = s->GetVariable("spotLight.enabled");
	
	s->SetVector3(spotLightid, 1, &color[0]);
	s->SetVector3(lightDirid, 1, &direction[0]);
	s->SetFloat(shininessid, shininess);
	s->SetVector3(spotPosid, 1, &position[0]);
	s->SetFloat(spotInCutid, cutOff);
	s->SetFloat(spotOutCutid, outerCutOff);
	s->SetFloat(conAttenID, 1.0);
	s->SetFloat(linAttenID, 0.0014);
	s->SetFloat(quadAttenID, 0.000007);
	if (isEnabled()) {
		s->SetFloat(spotLightEnable, 1.0);
	}
	else {
		s->SetFloat(spotLightEnable, -1.0);
	}
}

void SpotLight::setPosition(vec3 pos)
{
	position = pos;
}

void SpotLight::updateFlashLight(Camera* c)
{
	position = c->getPosition();
	direction = c->getFront();
}

SpotLight::~SpotLight()
{
}
