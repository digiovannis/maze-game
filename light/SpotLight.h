#pragma once
#include "Light.hpp"
#include "C:\\Users\\digio\\Documents\\Visual Studio 2015\\Projects\\CSC5210\\Project6\\Project6\\Camera.h"


class SpotLight :
	public Light
{
public:
	SpotLight(vec3 pos, vec3 col, vec3 dir, GLfloat incut, GLfloat outcut, GLfloat sh, GLfloat constantatt, GLfloat linearatt, GLfloat quadraticatt);
	~SpotLight();

	void connectLightToShader(Shader*);
	void setPosition(vec3 pos);
	void updateFlashLight(Camera* c);
private:
	vec3 direction;
	vec3 position;

	GLfloat shininess;
	
	GLfloat cutOff;
	GLfloat outerCutOff;
	GLfloat constant;
	GLfloat linear;
	GLfloat quadratic;
};

