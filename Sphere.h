#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include "DrawableObject.h"
#include "Shader.hpp"

class Sphere :
	public DrawableObject
{
public:
	Sphere();
	~Sphere();

	void draw(Shader* s);
private:
	GLfloat verts[72]; 
	
	GLuint VAO;
	GLuint VBO;

};

