#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <stdio.h>

using glm::vec3;
class BoundingBox
{
public:
	BoundingBox(vec3 _pos, float _length, float _width, float _depth);
	~BoundingBox();
	float getLength();
	float getWidth();
	float getDepth();
	vec3 getPosition();
	void setPosition(vec3 pos);
private:
	vec3 position;
	float length;
	float width;
	float depth;
};

