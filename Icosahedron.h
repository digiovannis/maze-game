#pragma once
#include "DrawableObject.h"
class Icosahedron :
	public DrawableObject
{
public:
	Icosahedron();
	void draw(Shader * shader);
	~Icosahedron();
private:
	GLsizei IndexCount;
	GLuint PositionSlot = 0;
};

