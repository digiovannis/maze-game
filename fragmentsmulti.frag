#version 330 core

// Variables coming in from the vertex shader
in vec3 color;
in vec3 Normal;
in vec3 FragPos;
in vec2 vs_tex_coord;//

out vec4 FragColor;

uniform sampler2D tex;

uniform vec3 EyePosition;//viewpos

uniform vec3 ambientColor;
uniform float ambientWeight;

struct DirLight {
    vec3 direction;

	float Shininess;
	float enabled;
	
	vec3 color;
    
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;

	float Shininess;

	float enabled;
  
    float constant;
    float linear;
    float quadratic;
  
    vec3 color;      
};


uniform DirLight dirLight;

uniform SpotLight spotLight;


//prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, vec3 Color);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 Color);


void main()
{
	//material color
	vec4 texColor = texture(tex, vs_tex_coord);
	vec3 Color = vec3(texColor.rgb);

	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(EyePosition - FragPos);

	//ambient
	vec3 ambient = ambientColor * Color;
	vec3 result;
	if(ambientWeight > 0.0)
		result = result + ambient * 0.2;

	//direct light
	if(dirLight.enabled > 0.0)
		result = result + CalcDirLight(dirLight, norm, viewDir, Color) * 0.5;

	//spot light
	if(spotLight.enabled > 0.0)
		result = result + CalcSpotLight(spotLight, norm, FragPos, viewDir, Color); 

	FragColor = vec4(result, 1.0);
	
	
}

// calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec3 Color)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), light.Shininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // combine results
    vec3 ambient = (light.color * Color);
    vec3 diffuse = (light.color * diff * Color);
    vec3 specular = (light.color * spec * Color);
    ambient = ambient * attenuation * intensity;
    diffuse = diffuse * attenuation * intensity;
    specular = specular * attenuation * intensity;
    return (diffuse + specular);//(ambient + diffuse + specular);
}

// calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, vec3 Color)
{
    vec3 lightDir = normalize(-light.direction);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), light.Shininess);
    // combine results
    vec3 ambient = (light.color * Color);
    vec3 diffuse = (light.color * diff * Color);
    vec3 specular = (light.color * spec * Color);
    return (ambient + diffuse + specular);
}