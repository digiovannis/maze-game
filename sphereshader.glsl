#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT {
    in vec3 color;
	in vec3 vFragPos;
} gs_in[];


out vec3 FragPos;
out vec3 color;
out vec3 Normal;

//uniform float time;
const float time = 1;

vec4 explode(vec4 position, vec3 normal) 
{
	float magnitude = 2.0;
    vec3 direction = 0; //normal * ((sin(time) + 1.0) / 2.0) * magnitude; 
    return position + vec4(direction, 0.0);
}

vec3 GetNormal() 
{
	vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
	vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
	return normalize(cross(a, b));
}

void main() {    
    vec3 normal = GetNormal();

    gl_Position = gl_in[0].gl_Position;//explode(gl_in[0].gl_Position, normal);
    Normal = normal;
	FragPos = gs_in[0].vFragPos;
    EmitVertex();
    gl_Position =gl_in[1].gl_Position; //explode(gl_in[1].gl_Position, normal);
    Normal = normal;
	FragPos = gs_in[1].vFragPos;
    EmitVertex();
    gl_Position = gl_in[2].gl_Position;//explode(gl_in[2].gl_Position, normal);
    Normal = normal;
	FragPos = gs_in[2].vFragPos;
    EmitVertex();
    EndPrimitive();
}  